(add-to-load-path (dirname (current-filename)))

(use-modules ((grid-printer) #:prefix grid:)
             )

#;(define (main)
  (let ([data
         '((("a" "b" "c") ("a" "b" "c") ("a" "b" "c"))
           (("1" "2" "3") ("1" "2" "3") ("1" "2" "3"))
           (("x" "y" "z") ("x" "y" "z") ("x" "y" "z")))])
    (grid:print-grid data
                     (current-output-port)
                     #:grid-config grid:default-grid-config)))

(define (main)
  (let ([data
         '((("a" "b b b" "c") ("a" "b" "c") ("a" "b" "c"))
           (("1" "2" "3") ("1" "2" "3 3 3") ("1" "2" "123"))
           (("x" "y" "z") ("x" "y" "z") ("x x" "y" "z")))]
        [custom-grid-config (grid:make-grid-config "|" "-" "+" " " 1 0 'left)])
    (grid:print-grid data
                     (current-output-port)
                     #:grid-config custom-grid-config)))

(main)
