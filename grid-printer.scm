;; TODO: remove this load path stuff
(add-to-load-path (dirname (current-filename)))

(define-module (grid-printer)
  #:version (0 2 0)
  #:export (print-grid
            equalize-lines-count
            <grid-config>
            make-grid-config
            default-grid-config))

(use-modules (helpers string-helpers)
             (helpers list-helpers)
             (srfi srfi-9 gnu))

;; =======
;; HELPERS
;; =======
(define (find-longest-string* lst)
  (find-longest* lst string-length))


(define identity
  (λ (sth) sth))


;; ===
;; LIB
;; ===
(define-immutable-record-type <grid-config>
  ;; define constructor
  (make-grid-config col-sep row-sep intersection empty
                    col-pad row-pad
                    pad-direction)
  ;; define predicate
  grid-config?
  ;; define accessors and functional setters
  (col-sep get-col-sep set-col-sep)
  (row-sep get-row-sep set-row-sep)
  (intersection get-intersection set-intersection)
  (empty get-empty set-empty)
  (col-pad get-col-pad set-col-pad)
  (row-pad get-row-pad set-row-pad)
  (pad-direction get-pad-direction set-pad-direction))

(define default-grid-config
  (make-grid-config
   #|col-sep|#
   "|"
   #|row-sep|#
   "-"
   #|intersection|#
   "+"
   #|empty|#
   " "
   #|col-pad|#
   1
   #|row-pad|#
   1
   #|pad-direction|#
   'left))

(define* (print-segmented-line
          seg-count seg-no-borders-width seg-filling seg-border
          #:optional (port (current-output-port)))
  (let loop ([segs-remaining seg-count])
    (cond [(> segs-remaining 0)
           (display seg-border port)
           (display (string-repeat seg-filling seg-no-borders-width) port)
           (loop (- segs-remaining 1))]
          [else (display seg-border port)
                (display "\n" port)])))

(define (col-content-width data-part-width grid-config)
  (+ (* 2 (get-col-pad grid-config)) data-part-width))

(define* (print-empty-line fields# data-part-width grid-config #:optional (port (current-output-port)))
  (print-segmented-line fields#
                        (col-content-width data-part-width grid-config)
                        (get-empty grid-config)
                        (get-col-sep grid-config)))


(define* (print-content-line min-field-count field-contents data-part-width grid-config
                             #:optional (port (current-output-port)))
  (cond
   [(null? field-contents)
    (print-empty-line min-field-count data-part-width grid-config port)]
   [else
    (let loop ([count min-field-count] [contents field-contents])
      (let ([col-content
             (string-join
              (list
               (string-repeat (get-empty grid-config)
                              (get-col-pad grid-config))
               (string-padding (if (null? contents)
                                   (get-empty grid-config)
                                   (car contents))
                               data-part-width
                               (get-empty grid-config)
                               #:padding-direction (get-pad-direction grid-config))
               (string-repeat (get-empty grid-config) (get-col-pad grid-config)))
              "")])
        (display (get-col-sep grid-config) port)
        (display col-content port))

      (if (> count 1)
          (loop (- count 1)
                (if (null? contents) '() (cdr contents)))
          (display (get-col-sep grid-config) port)))])
   (display "\n" port))


(define* (print-separating-line fields# data-part-width grid-config
                                #:optional (port (current-output-port)))
  (print-segmented-line fields#
                        (col-content-width data-part-width grid-config)
                        (get-row-sep grid-config)
                        (get-intersection grid-config)))


;; alias for better readability
(define output-padding-line print-empty-line)


;; When cells consist of lists of strings, those strings are supposed
;; to be printed on separate lines. When printing to a port, we have
;; to print line by line. In consequence we need to first print all
;; first parts, the first strings in those listsof strings, then the
;; second parts and so on. For that purpose of getting all nth parts
;; of the cells in a single list `get-nth-cell-parts` is defined here.
(define (get-nth-cell-parts cells cell-parts-ref)
  (map (λ (cell) (list-ref cell cell-parts-ref))
       cells))

(define (equalize-lines-count cells fill-elem)
  (let ([desired-len (longest-sublist-length cells)])
    (map (λ (cell-parts) (stretch-list cell-parts desired-len fill-elem))
         cells)))


;; A grid row are one or more lines of text which are printed between
;; separation lines.
(define* (print-grid-row row-data fields# data-part-width grid-config #:optional port)

  (define content-lines# (longest-sublist-length row-data))

  (define (iter-padding n)
    (cond [(> n 0)
           (output-padding-line fields# data-part-width grid-config port)
           (iter-padding (- n 1))]
          [else (display "" port)]))

  (define (iter-content cells)
    (let loop ([cell-parts-ref 0])
      (cond
       [(= cell-parts-ref content-lines#)
        (display "" port)]
       [else
        (let ([nth-cell-parts (get-nth-cell-parts cells cell-parts-ref)])
          (print-content-line fields# nth-cell-parts data-part-width grid-config port))
        (loop (+ cell-parts-ref 1))])))

  ;; Print the padding above the content.
  (iter-padding (get-row-pad grid-config))
  ;; Print the content in possibly arbitrary number of lines,
  ;; depending on cell-value-split-proc.
  (iter-content (equalize-lines-count row-data (get-empty grid-config)))
  ;; Print the padding below the content.
  (iter-padding (get-row-pad grid-config)))


(define* (print-grid data
                     #:optional (port (current-output-port))
                     #:key (grid-config default-grid-config))
  "The argument data is expected to be a list of rows, of which each
is a list of cells of which each is a list of strings or a simple
string."

  "print-grid requires the input to be 2-dimensional or 3-dimensional."

  "print-grid requires the input to be nested list of strings."

  (define fields# (longest-sublist-length data))
  (define longest-string-length (find-longest-string* data))
  (define (iter data)
    (cond
     [(null? data) (display "" port)]
     [else
      ;; print initial separating line, outer top border
      (print-separating-line fields# longest-string-length grid-config port)
      ;; Print one row of content.  This could result in multiple
      ;; content containing lines, depending on what
      ;; cell-value-split-proc does.
      (print-grid-row (car data) fields# longest-string-length grid-config port)
      ;; Continue with the next data point.
      (iter (cdr data))]))

  (cond
   [(member (dimendionality data) '(2 3))
    (let ([fields# (longest-sublist-length data)])
      (iter data)
      ;; final separating line, outer bottom border
      (print-separating-line fields# longest-string-length grid-config port))]
   [else
    (error "data dimendionality is not 1 or 2")]))
