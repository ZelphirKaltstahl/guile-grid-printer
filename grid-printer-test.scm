(add-to-load-path (dirname (current-filename)))

(use-modules (grid-printer)
             (helpers list-helpers)
             (helpers string-helpers)
             (srfi srfi-64))

(test-begin "grid-printer-test")

(test-group "dimensionality-test"
            (test-eqv 0 (dimendionality
                         '()))
            (test-eqv 1 (dimendionality
                         '(1 2 3)))
            (test-eqv 2 (dimendionality
                         '((1 2) (3 4))))
            (test-eqv 3 (dimendionality
                         '(1 2 (34 5 6) 3 (4 5 (3 ())))))
            (test-eqv 3 (dimendionality
                         '(1 2 (34 5 6 ()) 3 (4 5 (3 ())))))
            (test-eqv 3 (dimendionality
                         '(1 2 (34 5 6 (213)) 3 (4 5 (3 ())))))
            (test-eqv 3 (dimendionality
                         '(1 2 (34 5 6 (213 ())) 3 (4 5 (3 ())))))
            (test-eqv 4 (dimendionality
                         '(1 2 (34 5 6 (213 (1))) 3 (4 5 (3 ()))))))

(test-group "longest-sublist-length-test"
            (test-eqv 0 (longest-sublist-length
                         '()))
            (test-eqv 1 (longest-sublist-length
                         '(1 2 3 4 5 6)))
            (test-eqv 8 (longest-sublist-length
                         '((1 2 3 4 5 6 (a b c d (e) f g h)) a (b c d (1 2 3 4 5) e) c (d e f))))
            (test-eqv 6 (longest-sublist-length
                         '((1 2 3 4 5 6) a (b c d (1 2 3 4 5) e) c (d e f))))
            (test-eqv 5 (longest-sublist-length
                         '(a (b c d (1 2 3 4 5) e) c (d e f)))))

(test-group "equalize-lines-count-test"
            (let ([empty " "])
              (test-equal `(("1" "2" "3") ("a" ,empty ,empty) ("x" "y" ,empty))
                (equalize-lines-count '(("1" "2" "3") ("a") ("x" "y")) empty))
              (test-equal `(("a" ,empty ,empty) ("1" "2" "3") ("x" "y" ,empty))
                (equalize-lines-count '(("a") ("1" "2" "3") ("x" "y")) empty))
              (test-equal `(("a" ,empty ,empty) ("x" "y" ,empty) ("1" "2" "3"))
                (equalize-lines-count '(("a") ("x" "y") ("1" "2" "3")) empty))))

(test-end "grid-printer-test")
