(define-module (helpers string-helpers)
  #:export (string-repeat
            string-padding))

(define (string-repeat str n)
  (define (iter port str n)
    (cond
     [(> n 0)
      (display str port)
      (iter port str (- n 1))]
     [else ""]))

  (call-with-output-string
    (λ (port)
      (iter port str n))))


(define* (string-padding str width char #:key (padding-direction 'left))
  (let loop ((padded-string str))
    (cond
     [(< (string-length padded-string) width)
      (cond
       [(eq? padding-direction 'left) (loop (string-append char padded-string))]
       [(eq? padding-direction 'right) (loop (string-append padded-string char))]
       [else (error "padding-direction not left or right")])]
     [else padded-string])))
