(define-module (helpers list-helpers)
  #:export (dimendionality
            find-longest*
            longest-sublist-length
            stretch-list))

(use-modules (srfi srfi-64))

(define (empty-list? lst)
  (null? lst))

(define (atom? sth)
  (and (not (pair? sth))
       (not (empty-list? sth))))

(define (apply-to-items proc lst)
  (map proc lst))

(define (dimendionality lst)
  (define (iter lst depth)
    (cond
     [(null? lst) depth]
     [(pair? (car lst))
      (max (iter (car lst) (+ depth 1))
           (iter (cdr lst) depth))]
     [else
      (iter (cdr lst) depth)]))
  (cond
   [(null? lst) 0]
   [else (iter lst 1)]))


(define (find-longest* lst length-proc)
  (cond
   [(null? lst) 0]
   [(pair? (car lst))
    (max (find-longest* (car lst) length-proc)
         (find-longest* (cdr lst) length-proc))]
   [else
    (max (length-proc (car lst))
         (find-longest* (cdr lst) length-proc))]))

(define (longest-sublist-length lst)
  (define (iter longest lst)
    (cond
     [(null? lst) longest]
     [(atom? lst) 1]
     [(pair? (car lst))
      (max (iter 0 (car lst))
           (iter (+ 1 longest) (cdr lst)))]
     [else
      (iter (+ longest 1) (cdr lst))]))

  ;; only do it for items in the list, not for the list itself
  (cond
   [(null? lst) 0]
   [else
    (apply max
           (apply-to-items (λ (elem) (iter 0 elem))
                           lst))]))


#;(define (create-vector-length-n-from-list lst desired-length filling-element)
  (let ([vec-to-fill (make-vector desired-length filling-element)])
    (let iter ([vec-index 0] [remaining lst])
      (cond
       [(= vec-index desired-length) vec-to-fill]
       [(null? remaining)
        (vector-set! vec-to-fill vec-index filling-element)
        (iter (+ vec-index 1) '())]
       [else
        (vector-set! vec-to-fill vec-index (car remaining))
        (iter (+ vec-index 1) (cdr remaining))]))))


(define (list->list-of-len lst desired-length fill-elem)
  (define (iter remaining elem-counter)
    (cond
     [(= elem-counter desired-length) '()]
     [(null? remaining)
      (cons fill-elem
            (iter '() (+ elem-counter 1)))]
     [else
      (cons (car remaining)
            (iter (cdr remaining) (+ elem-counter 1)))]))
  (iter lst 0))


(define (stretch-list lst desired-length fill-elem)
  (let ([vector-to-fill (make-vector desired-length fill-elem)])
    (list->list-of-len lst desired-length fill-elem)))

  ;; (define (iter orig-lst elem-count)
  ;;   (cond
  ;;    [(< elem-count desired-length)
  ;;     (cond
  ;;      [(null? orig-lst)
  ;;       (cons (iter '() (+ elem-count 1)) filling-elem))]
  ;;      [else ...])]
  ;;    [else '()]))
  ;; (iter lst '() 0))
